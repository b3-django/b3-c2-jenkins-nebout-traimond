const arguments = process.argv;
const argCalc = arguments.slice(2);
const calcul = argCalc[0];


module.exports = {
    detectValidArgument,
    detectValidCharactersInCalcul,
    detectGobalPotentialErrors,
    checkParenthesisValidity,
    areParenthesisOperationsValid,
    getParenthesisContent,
    opPlus,
    opMinus,
    opMultiply,
    opDivide,
    //opPower,
    opPercent,
    //opSquare,
    //opSquareRoot,
};

function detectValidArgument(argument) {
    if (argument.length > 1) {
        errorMsg = "Console msg - Trop d'arguments";
        console.log(errorMsg)
        return false;
    }
    return true;
}

function detectValidCharactersInCalcul(argCalcul) {
    const validCharacters = /^[\(\)\/\*\-\+\^\.\%\√\d]*$/;
    result = validCharacters.test(argCalcul);
    if (!result) {
        errorMsg = "Console msg - Le calcul contient des caractères non-valides";
        console.log(errorMsg)
    }
    return result;
}

function detectGobalPotentialErrors(argCalcul) {
    const openParenthesisPattern = /[(]/g;
    const closeParenthesisPattern = /[)]/g;

    var countOpenParenthesis = argCalcul.match(openParenthesisPattern) || 0;
    var countCloseParenthesis = argCalcul.match(closeParenthesisPattern) || 0;

    if (countOpenParenthesis.length != countCloseParenthesis.length) {
        errorMsg = "Console msg - Les parenthèses ne sont pas égales";
        console.log(errorMsg);
        return false;   
    }

    const validOperatorPattern = /[\/\*\-\+\^\.\%\√]{2,}/g;
    var areOperatorValid = argCalcul.match(validOperatorPattern) || 0;
    if (areOperatorValid.length > 0) {
        errorMsg = "Console msg - Erreur d'opérateur " + areOperatorValid;
        console.log(errorMsg);
        return false;
    }
    return true;
}


function checkParenthesisValidity(calcul) {
    const parenthesisPattern = /[(|)]/g;
    const thereIsParenthsis = calcul.match(parenthesisPattern) || 0;

    if (thereIsParenthsis == 0) {
        return false;
    }

    let listOfParenthesis = [];

    for (let index = 0; index < calcul.length; index++) {
        var currentElement = calcul[index];
        if (index < 1) {
            var previousElement = '§';
        } else {
            var previousElement = calcul[index - 1];
        }

        if (index == calcul.length - 1) {
            var nextElement = '§';
        } else {
            var nextElement = calcul[index + 1];
        }

        if (currentElement == '(') {
            listOfParenthesis.push({ 'position': index, 'parenthesis': currentElement, 'previous': previousElement, 'next': nextElement });
        }
        if (currentElement == ')') {
            listOfParenthesis.push({ 'position': index, 'parenthesis': currentElement, 'previous': previousElement, 'next': nextElement });
        }
    }
    const parenthesisValidity = areParenthesisOperationsValid(listOfParenthesis);
    return parenthesisValidity;

}

function areParenthesisOperationsValid(listOfParenthesis) {

    const nextToOpenParenthesisPattern = /[\(\-\+√\d]/g; // (
    const previousToOpenParenthesisPattern = /[\(\/\*\-\+\^\%\§]/g;
    const nextToCloseParenthesisPattern = /[\)\/\*\-\+\^\%\√\§]/g; // )
    const previousToCloseParenthesisPattern = /[\)\d]/g;

    var parenthesisOperatorsValidity = listOfParenthesis.every((parInfos, i) => {
        var position = parInfos.position;
        var parenthesis = parInfos.parenthesis;
        var next = parInfos.next;
        var previous = parInfos.previous;

        switch (parenthesis) {
            case '(':
                var nextPatternValidation = next.match(nextToOpenParenthesisPattern) || 0;
                var previousPatternValidation = previous.match(previousToOpenParenthesisPattern) || 0;
                break;
            case ')':
                var nextPatternValidation = next.match(nextToCloseParenthesisPattern) || 0;
                var previousPatternValidation = previous.match(previousToCloseParenthesisPattern) || 0;
                break;
        }

        if (previousPatternValidation == 0 || nextPatternValidation == 0) {
            return false;
        }
        return true;

    });

    if (!parenthesisOperatorsValidity) {
        errorMsg = "Console msg - Erreur dans les opérations ";
        console.log(errorMsg);
        return false;
    }

    return true;
}

function getParenthesisContent(calcul, listOfParenthesis) {
    parGroups = 0;
    parOpen = 0;
    parClosed = 0;

    for (let i = 0; i < calcul.length; i++) {
        var element = calcul[i];
        switch (element) {
            case '(':
                parOpen++
                break;
            case ')':
                parClosed++
                break;
        }

        if (parOpen == parClosed) {

        }
    }
}


const isArgValid = detectValidArgument(argCalc);
if (isArgValid) {
    const isCalcValid = detectValidCharactersInCalcul(calcul);
    if (isCalcValid) {
        const gobalPotentialErrors = detectGobalPotentialErrors(calcul);
        if (gobalPotentialErrors) {
            const parValidity = checkParenthesisValidity(calcul);
            /* const formatCalc = calcul.replace(/[\^]/g, '**');
            console.log(formatCalc);
            console.log(eval(formatCalc)); */
        }
    }
}

function opPlus(x, y) {
    x = parseFloat(x);
    y = parseFloat(y);
    z = x + y;
    return z;
}


/*function resultOperations (argCalcul) {
    let pattern = /([-+]?\d?\.?\d+)([-+*%^\/]+([-+]?\d+\.?\d*))+/;
    let test = calcul.match(pattern);
    console.log(test);  
}

resultOperations();
*/

function opMinus(x, y) {
    x = parseFloat(x);
    y = parseFloat(y);
    z = x - y;
    return z;
}

function opDivide(x, y) {
    x = parseFloat(x);
    y = parseFloat(y);
    z = x / y;
    return z;
}

function opMultiply(x, y) {
    x = parseFloat(x);
    y = parseFloat(y);
    z = x * y;
    return z;
}

function opPercent(x, y) { // x=90,y=60 => 90% de 60 = 54
    x = parseFloat(x) / 100;
    y = parseFloat(y);
    z = x * y;
    return z;
}