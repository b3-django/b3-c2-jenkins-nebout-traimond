var assert = require('assert');
const {
    detectValidArgument,
    detectValidCharactersInCalcul,
    detectGobalPotentialErrors,
    checkParenthesisValidity,
    areParenthesisOperationsValid,
    getParenthesisContent,
    opPlus,
    opMinus,
    opMultiply,
    opDivide,
    opPercent,
} = require('../app.js');



describe('Calculator Tests', () => {

    describe('Argument Validity', () => {

        it('Should be true - Test if there is only 1 argument with 1 argument', () => {
            var test = ['element1'];
            var result = detectValidArgument(test);
            assert.equal(result, true)
        })


        it('Should be false - Test if there is 1 argument with 2+ arguments', () => {
            var test = ['element1', 'element2'];
            var result = detectValidArgument(test);
            assert.equal(result, false)
        })


    })


    describe('Calcul String Validity', () => {

        it('Should be true - string containing only valid charaters', () => {
            var test = "0123456789.+-*/%^√";
            var result = detectValidCharactersInCalcul(test);
            assert.equal(result, true)
        })


        it('Should be false - string containing invalid charaters', () => {
            var test = "0123456789.+-*/%^√AZERTY&";
            var result = detectValidCharactersInCalcul(test);
            assert.equal(result, false)
        })

        it('Should be true - string containing equal amout of ( and )', () => {
            var test = "(12+2)*(13+3)";
            var result = detectGobalPotentialErrors(test);
            assert.equal(result, true)
        })

        it('Should be false - string NOT containing equal amout of ( and )', () => {
            var test = "((12+2)*((13+3)";
            var result = detectGobalPotentialErrors(test);
            assert.equal(result, false)
        })


        it('Should be true - contain a valid calc', () => {
            var test = "(12+2)*(13+3)";
            var result = checkParenthesisValidity(test);
            assert.equal(result, true)
        });

        it('Should be false - contain an invalid calc * directly after (', () => {
            var test = "(12+2)^(*13+3)";
            var result = checkParenthesisValidity(test);
            assert.equal(result, false)
        });
    })



    describe('Operations Validity', () => {

        describe('Plus', () => {
            it('Should be true - -12+13.5=1.5', () => {
                var x = -12;
                var y = 13.5;
                var result = opPlus(x, y);
                assert.equal(result, 1.5)
            })

            it('Should be false - 15+13.5=-5', () => {
                var x = 15;
                var y = 13.5;
                var result = opPlus(x, y);
                assert.notEqual(result, -5)
            })
        })

        describe('Minus', () => {
            it('Should be true - 5-3=2', () => {
                var x = 5;
                var y = 3;
                var result = opMinus(x, y);
                assert.equal(result, 2)
            })

            it('Should be false - 5-10=-9', () => {
                var x = 5;
                var y = -10;
                var result = opMinus(x, y);
                assert.notEqual(result, -9)
            })
        })


        describe('Multiply', () => {
            it('Should be true - 5*-3=-18.48', () => {
                var x = 5;
                var y = -3;
                var result = opMultiply(x, y);
                assert.equal(result, -15)
            })

            it('Should be false - 13*-12=0', () => {
                var x = 13;
                var y = -12;
                var result = opMultiply(x, y);
                assert.notEqual(result, 0)
            })
        })

        describe('Divide', () => {
            it('Should be true - 3/3=1', () => {
                var x = 3;
                var y = 3;
                var result = opDivide(x, y);
                assert.equal(result, 1)
            })

            it('Should be false - 13/-12=0', () => {
                var x = 13;
                var y = -12;
                var result = opDivide(x, y);
                assert.notEqual(result, 0)
            })
        })


        describe('Percent', () => {
            it('Should be true - 15.5%100=15.5', () => {
                var x = 15.5;
                var y = 100;
                var result = opPercent(x, y);
                assert.equal(result, 15.5)
            })

            it('Should be false - 20%400=50', () => {
                var x = 20;
                var y = 400;
                var result = opPercent(x, y);
                assert.notEqual(result, 50)
            })
        })
    })


})